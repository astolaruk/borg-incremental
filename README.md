### borg-incremental Backup Script

I use this script to perform incremental 3-2-1 methodology backups using borg (when appropriate)

The script is designed to maintain an identical local and remote borg backup vault.

By default, it will build vaults that keep
* 7 daily backups
* 4 weekly backups
* 13 monthly backups
* 1 yearly backup

It prunes and checks the intergrity of the vaults on every run. It emails you a log file when it's done.