#!/bin/bash

# Be sure to install mailutils if you want email notifications
# Obviously, be sure to have borgbackup installed first!

### BASIC SETTINGS
	# Cosmetic name for this backup job
	backupname="Borg Incremental Backup"
	# Where the local backup vault should be stored
	workdir=$HOME/borg-backup

### NOTIFICATION SETTINGS
	# Set to 1 to enable email notifications
	sendemail=1
	# Email address to send notifications to
	adminemail="root@localhost"
	# Email address to send notifications from
	fromemail="backup@localhost"

### REMOTE BACKUPS
	# Set to 1 to enable remote backup
	do_remote_backup=0
	# Server to send remote backups to (if using)
	remoteserver="someuser@someserver"
	# Locaton of backups on the remote server (default: same as local, put it in the home directory)
	remotepath=$workdir

### ADVANCED SETTINGS
	# Date stamp to append to backups and logs (default ISO-8601)
	date=`date "+%Y-%m-%dT%H%M%S"`
	# Where the logfiles should be created
	logpath=$workdir/logs
	# What the logfile should be named
	logname="backup.$date.txt"

# Output rsync exit code and check for errors
errorcheck()
{
        exitcode=$(echo $?)
        echo "" >> $logfile
        echo "EXIT CODE: $exitcode" >> $logfile
        echo "" >> $logfile

        # If a non-zero exit code is returned, append ERROR to the subject line
        if [[ $exitcode -ne 0 ]] && [[ ! $backupname == *"ERRORS"* ]]
        then
                backupname="ERRORS - $backupname"
        fi
}

### LOCAL BACKUP

# Check to see if the backup directory structure is created, if not then create it
	if [ ! -d $workdir ]; then mkdir -p $workdir; fi
	if [ ! -d $workdir/logs ]; then mkdir $workdir/logs; fi

# Borg init
	borg init --encryption=none $workdir/vault

# Create a new log file
	logfile=$logpath/$logname
	/bin/echo "Backup: $backupname back-$date" > $logfile
	/bin/echo "" >> $logfile

# Perform the local backup
	/bin/echo "--- LOCAL BACKUP ---" >> $logfile
	/bin/echo "" >> $logfile

	borg create -s -v $workdir/vault::$date \
	/home \
	/etc \
	/var/www \
	--exclude /etc/ssl/private \
	2>> $logfile >> $logfile
	errorcheck

	/bin/echo "Running integrity check..." >> $logfile
	borg check $workdir/vault
	errorcheck

	/bin/echo "Deleting old backups..." >> $logfile
	# By default, this script keeps 7 daily backups, 4 weekly, 13 monthly, and 1 yearly
	borg prune -v -d=7 -w=4 -m=13 -y=1 --list $workdir/vault >> $logfile
	errorcheck

        /bin/echo "--- List of backups ---" >> $logfile
        borg list $workdir/vault >> $logfile
        errorcheck

### REMOTE BACKUP

if [ $do_remote_backup -eq 1 ]; then

	ssh $remoteserver "mkdir -p $remotepath"
	borg init --encryption=none $remoteserver:$workdir/vault

	/bin/echo "" >> $logfile
	/bin/echo "--- REMOTE BACKUP ---" >> $logfile
	/bin/echo "" >> $logfile

	# Send the remote backup

	borg create -s -v $remoteserver:$workdir/vault::$date \
	/home \
	/etc \
	/var/www \
	--exclude /etc/ssl/private \
	2>> $logfile >> $logfile
	errorcheck

        /bin/echo "Running integrity check..." >> $logfile
        borg check $remoteserver:$workdir/vault
        errorcheck

        /bin/echo "Pruning old backups..." >> $logfile
        borg prune -v -d=7 -w=4 -m=13 -y=1 --list $remoteserver:$workdir/vault >> $logfile
        errorcheck

        /bin/echo "--- List of backups ---" >> $logfile
        borg list $remoteserver:$workdir/vault >> $logfile
        errorcheck

fi

# Show the job duration
	duration=$SECONDS
	echo "DURATION: $(($duration / 60 / 60))h $(($duration / 60 % 60))m $(($duration % 60))s" >> $logfile

if [ $sendemail -eq 1 ]; then
	# Email the log file
	/bin/echo | /bin/cat $logfile | mail -s "$backupname Borg Backup Job" -a "From: $fromemail" "$adminemail"
fi
